﻿using System;

namespace week5_ex26_pt2
{
    class Program
    {
        static void Main(string[] args)
        {
            var student1 = new Student("Jesse", 12);
            var student2 = new Student("Kadin", 19);
            Console.WriteLine(student1.SetStudent);
            Console.WriteLine(student2.SetStudent);

        }
        private class Student
        {
            private string studentName;
            private int studentId;

            public Student(string _name, int _id)
            {
                studentName = _name;
                studentId = _id;
            }
            public string SetStudent
            {
                get
                {
                    var a = $"Your name is {studentName}. ";
                    var b = $"Your StudentID is #{studentId}"; 
                    return a+b;
                     
                }
                
                
                
            }
        }
    }
}
